const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GeoPointSchema = new Schema({
	lat: { type: Number },
	lon: { type: Number },
});

module.exports = mongoose.model('GeoPoint', GeoPointSchema);
