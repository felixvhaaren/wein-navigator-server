const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FoodSchema = new Schema({
    name: String,
    price: Number,
});

module.exports = mongoose.model('Food', FoodSchema);