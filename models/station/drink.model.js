const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DrinkSchema = new Schema({
    year: Number,
    name: String,
    category: {type: String},
    desc: String,
    alcohol: Number,
    prices: [
        {    
            price: Number,
            volume: Number,
        }
    ]
});

module.exports = mongoose.model('Drink', DrinkSchema);