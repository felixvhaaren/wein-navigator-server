const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const GeoPoint = require('../geoPoint.model').schema;

const StationSchema = new Schema({
	companies: [{ type : mongoose.Schema.Types.ObjectId, ref: 'Company' }],
	toilet: {type: Boolean, default: false},
	lat: Number, 
	lon: Number,
	sortIndex: Number,
});

module.exports = mongoose.model('Station', StationSchema);
