const mongoose = require('mongoose');

const CompanySchema = new mongoose.Schema(
	{
		user: { type: mongoose.Schema.ObjectId, ref: 'User' },
		name: { type: String, unique: true, required: true },
		tel: { type: String },
		email: { type: String },
		website: { type: String },
		facebook: { type: String, required: true },
		twitter: { type: String, required: true },
		instagram: { type: String, required: true },
		street: String,
		zip: Number,
		city: String,
		logo: String,
		banner: { type: String, default: '' },
		itemType: String,
	},
	{ discriminatorKey: 'itemType' }
);

CompanySchema.methods.toWeb = function() {
	let json = this.toJSON();
	json.id = this._id; //this is for the front end
	return json;
};

const Company = mongoose.model('Company', CompanySchema);

const Winegrower = Company.discriminator(
	'Winegrower',
	new mongoose.Schema({
		drinks: { type: Array, default: [], ref: 'Drink' },
		food: { type: Array, default: [], ref: 'Food' },
	})
);

const Band = Company.discriminator(
	'Band',
	new mongoose.Schema({
		performances: [
			{
				stationName: { type: String },
				eventDate: {
					start: { type: Number, required: true },
					end: { type: Number, required: true },
				},
			},
		],
	})
);

module.exports = Company;
