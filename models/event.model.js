const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const GeoPoint = require('./geoPoint.model').schema;
const Station = require('./station/station.model').schema;
const Company = require('./company.model').schema;
const { to, ReE, ReS } = require('../services/util.service');

const EventSchema = new Schema(
	{
		name: { type: String, required: true },
		category: {type: String, required: true},
		eventDate: {
			start: { type: Number, required: true },
			end: { type: Number, required: true },
		},
		previewUrl: { type: String, required: true },
		imageUrl: { type: String, required: true },
		eventLocation: {
			lat: {type: Number, required: true},
			lon: {type: Number, required: true},
			zip: { type: Number, required: true },
			city: { type: String, required: true }
		},
		description: {type: String, required: true},
		stations: [Station],
		parking: [{lat: Number, lon: Number, parkType: String}],
		organizer: {type: mongoose.Schema.ObjectId, ref: 'Company', required: true},
		user: { type: mongoose.Schema.ObjectId, ref: 'User' },
	},
	{ timestamps: true }
);

EventSchema.methods.toWeb = function() {
	let json = this.toJSON();
	json.id = this._id; //this is for the front end
	return json;
};

module.exports = mongoose.model('Event', EventSchema);
