const { Event } = require('../models');
const Company = require('../models/company.model');
const { to, ReE, ReS } = require('../services/util.service');

const create = async function(req, res) {
	res.setHeader('Content-Type', 'application/json');
	let err, event;
	let user = req.user;

	let event_info = req.body;
	event_info.user = user._id;

	[err, event] = await to(Event.create(event_info));
	if (err) return ReE(res, err, 422);

	return ReS(res, { event: event.toWeb() }, 201);
};
module.exports.create = create;

const getAll = async function(req, res) {
	res.setHeader('Content-Type', 'application/json');
	let user_events_json = [];
	let events_json = [];
	if (req.user) {
		let user = req.user;
		let err, events;
		//get events where user is admin
		[err, events] = await to(user.Events());
		for (let i in events) {
			let event = events[i];
			user_events_json.push(event.toWeb());
		}
	}
	//get all events
	[err, events] = await to(Event.find({}).populate('organizer').populate("stations.companies"));

	for (let i in events) {
		let event = events[i];
		events_json.push(event.toWeb());
	}

	return ReS(res, events_json);
};
module.exports.getAll = getAll;

const get = async function(req, res) {
	res.setHeader('Content-Type', 'application/json');
	let event_id, err, event;
	event_id = req.params.event_id;

	[err, event] = await to(
		Event.findOne({ _id: event_id }).populate("organizer").populate("stations.companies")
	);

	if (err) return ReE(res, 'err finding event');
	if (!event) return ReE(res, 'Event not found with id: ' + event_id);

	return ReS(res, event.toWeb());
};
module.exports.get = get;

const update = async function(req, res) {
	let err, event, data;
	event = req.user;
	data = req.body;
	event.set(data);

	[err, event] = await to(event.save());
	if (err) {
		return ReE(res, err);
	}
	return ReS(res, { event: event.toWeb() });
};
module.exports.update = update;

const remove = async function(req, res) {
	let event, err;
	event = req.event;

	[err, event] = await to(event.remove());
	if (err) return ReE(res, 'error occured trying to delete the event');

	return ReS(res, { message: 'Deleted Event' }, 204);
};
module.exports.remove = remove;
