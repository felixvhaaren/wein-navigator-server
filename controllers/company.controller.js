const Company = require('./../models/company.model');
const { to, ReE, ReS } = require('../services/util.service');

const create = async function(req, res) {
	res.setHeader('Content-Type', 'application/json');
	let err, company;
	let user = req.user;
	let company_info = req.body;
	if (user) {
		company_info.user = user._id;
	}

	[err, company] = await to(Company.create(company_info));
	if (err) return ReE(res, err, 422);

	return ReS(res, { Company: company.toWeb() }, 201);
};

const get = async function(req, res) {
	res.setHeader('Content-Type', 'application/json');
	let company_id, err, company;
	company_id = req.params.company_id;

	[err, company] = await to(
		Company.findOne({ _id: company_id })
	);

	if (err) return ReE(res, 'err finding company');
	if (!company) return ReE(res, 'Company not found with id: ' + company_id);

	return ReS(res, company.toWeb());
};
module.exports.get = get;

module.exports.create = create;
