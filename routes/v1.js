const express = require('express');
const router = express.Router();

const UserController = require('../controllers/user.controller');
const EventsController = require('../controllers/event.controller');
const CompaniesController = require('../controllers/company.controller');
const custom = require('./../middleware/custom');

const passport = require('passport');

require('./../middleware/passport')(passport);

optionalAuth = (req, res, next) => {
	const auth = req.header('Authorization');
	if (auth) {
		passport.authenticate('jwt', { session: false }, function(
			err,
			user,
			info
		) {
			req.user = user;
			next();
		})(req, res, next);
	} else {
		next();
	}
};

/* GET home page. */
router.get('/', (req, res, next) => {
	res.json({
		status: 'success',
		message: 'Wein Navigator API',
		data: { version_number: 'v1.0.0' },
	});
});

//Should need admin auth
router.post('/users', UserController.create); // C
router.get(
	'/users',
	passport.authenticate('jwt', { session: false }),
	UserController.get
); // R
router.put(
	'/users',
	passport.authenticate('jwt', { session: false }),
	UserController.update
); // U
router.delete(
	'/users',
	passport.authenticate('jwt', { session: false }),
	UserController.remove
); // D
router.post('/users/login', UserController.login);

/* EVENTS */
router.post(
	'/events',
	passport.authenticate('jwt', { session: false }),
	EventsController.create
); // C
router.get('/events', optionalAuth, EventsController.getAll);
// R
router.get('/events/:event_id', EventsController.get); // R
router.put(
	'/events/:event_id',
	passport.authenticate('jwt', { session: false }),
	custom.event,
	EventsController.update
); // U
router.delete(
	'/events/:event_id',
	passport.authenticate('jwt', { session: false }),
	custom.event,
	EventsController.remove
); // D

/* COMPANY */
router.post('/companies', optionalAuth, CompaniesController.create);
router.get('/companies/:company_id',optionalAuth, CompaniesController.get); // R

//********* API DOCUMENTATION **********
/*router.use('/docs/api.json',            express.static(path.join(__dirname, '/../public/v1/documentation/api.json')));
router.use('/docs',                     express.static(path.join(__dirname, '/../public/v1/documentation/dist')));*/
module.exports = router;
