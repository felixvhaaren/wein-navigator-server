const Event = require('../models/event.model');
const { to, ReE } = require('../services/util.service');
const jwt = require('jsonwebtoken');
const CONFIG = require('../config/config');

/* get event by id*/
let event = async function(req, res, next) {
	let event_id, err, event;
	event_id = req.params.event_id;

	[err, event] = await to(Event.findOne({ _id: event_id }));
	if (err) return ReE(res, 'err finding event');

	if (!event) return ReE(res, 'Event not found with id: ' + event_id);
	let user, users_array;
	user = req.user;
	users_array = event.users.map(obj => String(obj.user));

	if (!users_array.includes(String(user._id)))
		return ReE(
			res,
			'User does not have permission to read app with id: ' + app_id
		);

	req.event = event;
	next();
};
module.exports.event = event;

let parseJwt = async function (req, res, next){
	console.log(req.headers.authorization.replace('Bearer ', ''));
	
	jwt.verify(req.headers.authorization.replace('Bearer ', ''), CONFIG.jwt_encryption, (err, decoded) => {
		if (err) return ReE(res, 'err parsing user');
		console.log(decoded);
		req.user = decoded;
	});

	next();
}

module.exports.parseJwt = parseJwt;
