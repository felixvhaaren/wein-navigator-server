require('dotenv').config(); //instatiate environment variables

let CONFIG = {}; //Make this global to use all over the application

CONFIG.app = process.env.APP || 'development';
CONFIG.port = process.env.PORT || '3000';

CONFIG.db_dialect = process.env.DB_DIALECT || 'mongo';
CONFIG.db_host = process.env.DB_HOST || 'localhost';
CONFIG.db_port = process.env.DB_PORT || '27017';
CONFIG.db_name = process.env.DB_NAME || 'name';
CONFIG.db_user = process.env.DB_USER || 'root';
CONFIG.db_password = process.env.DB_PASSWORD || 'db-password';

CONFIG.jwt_encryption = process.env.JWT_ENCRYPTION || 'GgpPNQH8hjY85Z_dr-Fk-xJxgHGM50uBiJRICgcx_aJPlS-MREqO-Dczm8wW2JKQK8b74PPrIeqqstmkdRTWWHgW8XC0j8Lq9EWVsNOco_FEl9hKfXG29wq7PJbi0_tfbthd3vSNbpNrv2uXF4sFzuMLV1M1Qe1ZwZqoy28JMljjn0nFpY8P-2hALPBvsaDJwhgByDDGv_pX-fwLwfVjbAJdH94fMlQG-w7tnQQ7iQTbKdKfJiXfKl7II-VBO7JybE5Yab4ENH3TK8lPmw_Hw2rYEtQaxdYZsi7ssVsQwBo0RyXp4n_6gL3Jn0r1HZzvskSqqK36nqgLeyY17DLMAQ';
CONFIG.jwt_expiration = process.env.JWT_EXPIRATION || '1000000';

module.exports = CONFIG;
